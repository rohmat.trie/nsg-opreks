#!/usr/bin/python3

import os
import argparse
import glob
import jinja2

working_dir = os.getcwd()


# def get_args():
#     parser = argparse.ArgumentParser()
#     parser.add_argument("--source-prefix", dest="ap", help ="Source IP Address ex: 10.1.140.0/24, ServiceTags ")
#     parser.add_argument("--subscription", dest="ap", help ="Source IP Address ex: 10.1.140.0/24, ServiceTags ")
#     parser.add_argument("--destination-address")


#/subscriptions/yyyy-wwww-xxx-12341341/resourceGroups/rg-sec-preprod/providers/Microsoft.Network/applicationSecurityGroups/asg-test-1

def generate_rule():
    
    global subscription, rule_name, protocol, destination, destination_var, destination_port, source, source_var, source_type, dest_type
    
    subscription = input ("Subscription : ")
    rule_name = input("rule name : ")
    protocol = input("Protocol : ")
    source_type = input("Source Type : [ 1.Application Security Group, 2.Service Tags, 3.IP Address  ] : ")
    
    if source_type == "1":
        source = input("Application Security Group : ")
    elif source_type == "2":
        source = input("Service Tag : ")
    else:
        source = input("IP Address : ")

    source_var = input("Source Variable : ")
    dest_type = input("Destination Type : [ 1.ASG, 2.Virtual Network, 3.IP Address  ] : ")
    
    if dest_type == "1":
        destination = input("Application Security Group : ")
    elif dest_type == "2":
        destination = input("VirtualNetwork : ")
    else:
        destination = input("IP Address : ")

    destination_var = input("Destination Variable: ")
    destination_port = input("Destination Port : ")
    
    #Template
    template_loader = jinja2.FileSystemLoader(searchpath="./")
    template_env = jinja2.Environment(loader=template_loader)
    if source_type == '3' and dest_type == '3':
        template_file = "template/nsg-1.yaml.j2"
    elif source_type == '1'  and dest_type =='1':
        template_file = "template/nsg-2.yaml.j2"
    else:
        template_file = "template/nsg-3.yaml.j2"
        
    template = template_env.get_template(template_file)
    
    output_text = template.render(
        rule_name=rule_name,
        protocol=protocol,
        source=source,
        source_var=source_var,
        destination=destination,
        destination_var=destination_var,
        destination_port=destination_port
        )
    
    return output_text
        
def nsg_var_preprod():
    folder_path = working_dir + "/*/infra-network-preproduction/*/group_vars/*"
    
    for name in glob.glob(folder_path):
        
        s = name.rstrip('home')
        print (s)
        
def generate_task():
    n1 = generate_rule()
    if subscription == "sub-preprod-lognet":
        loc_path = working_dir + "/playbooks/create-nsg-market-1.yaml"
    else:
        loc_path = working_dir + "/playbooks/create-nsg-cicd-1.yaml"
    
    #Create condition to skip if the rules already exists
    with open(loc_path, 'a') as g:
        g.write("\n\n" + n1)
    
    return g
    
def generate_preprod_variable():
    
    var_lognet = working_dir + "/inventories/infra-network-preproduction/lognet/group_vars/nsg.yaml"
    var_cicd = working_dir + "/inventories/infra-network-preproduction/cicd/group_vars/nsg.yaml"
    
    if subscription == "sub-preprod-lognet":
        with open(var_lognet, 'a') as f_log:
            f_log.write("\n" + source_var + ": " + source)
            f_log.write("\n" + destination_var + ": " + destination)
    else:
        with open(var_cicd, 'a') as  f_cicd:
            f_cicd.write("\n" + source_var + ": " + source)
            f_cicd.write("\n" + destination_var + ": " + destination)


def generate_prod_variable():
    
    var_lognet = working_dir + "/inventories/infra-network-production/lognet/group_vars/nsg.yaml"
    var_cicd = working_dir + "/inventories/infra-network-production/cicd/group_vars/nsg.yaml"
    
    if subscription == "sub-prod-lognet":
        with open(var_lognet, 'a') as f_log:
            f_log.write("\n" + source_var + ": " + source)
            f_log.write("\n" + destination_var + ": " + destination)
    else:
        with open(var_cicd, 'a') as  f_cicd:
            f_cicd.write("\n" + source_var + ": " + source)
            f_cicd.write("\n" + destination_var + ": " + destination)


def main():
    generate_task()
    generate_preprod_variable()

if __name__ == '__main__':
    main()
    #nsg_var_preprod()
